import { AuthContext } from '../../context/AuthContext'
import React, { useState, useContext } from 'react'
import { View, Text, ScrollView, Image, StyleSheet, Button, TouchableOpacity } from 'react-native'
import { Dimensions } from 'react-native'

import CustomInput from '../../components/misc/CustomInput'
import CustomLoginButton from '../../components/misc/CustomLoginButton'
import GoogleSignInButton from '../../components/SignInSpecial/GoogleSignInButton'
import FaceBookSignInButton from '../../components/SignInSpecial/FaceBookSignInButton'



const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const LoginScreen = ({ navigation }) => {

    const {login} = useContext(AuthContext) 


    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")  
    
    
  return (
    <ScrollView style={{backgroundColor: "white"}}> 
        <View style={styles.LoginScreenView}>
        
            {/* img */}
            <View style={styles.authLogoContainer}>
                <Image source={require('../../img/logo.jpg')} style={styles.logo}/>
                <Text style={styles.logoTitle}>Sign in</Text>
            </View>

            <View style={styles.registerHereContainer}>
                    <TouchableOpacity
                        onPress={ () => navigation.navigate('Home')}
                        style={styles.registerHereLink}
                    >
                        <Text style={styles.forgottenHere}>
                            Go Home
                        </Text>
                    </TouchableOpacity>
                </View>      


            {/* form login */}
            <View style={styles.formContainer}>  

                <CustomInput 
                    placeholder={"email"} 
                    value={email}
                    onChangeText={(text) => setEmail(text)}       
                             
                />
            
                <CustomInput 
                    placeholder={"Password"} 
                    value={password} 
                    onChangeText={(text) => setPassword(text)} 
                    secureTextEntry={true}                
                />

                <TouchableOpacity
                    // onPress={()=> console.log(email, password)}                        
                    onPress={()=>login()}                        
                >                       
                    <Text style={styles.loginBtn}>Login </Text>
                </TouchableOpacity>
            
                

                <View style={styles.registerHereContainer}>
                    <TouchableOpacity
                        onPress={ () => navigation.navigate('ForgotPassword')}
                        style={styles.registerHereLink}
                    >
                        <Text style={styles.forgotten}>Password forgotten ? </Text>
                        <Text style={styles.forgottenHere}>We got you here 👽 </Text>
                    </TouchableOpacity>
                </View>      

            </View>        

            <View>
                {/* login with google button */}
                <GoogleSignInButton />

                {/* login with facebook button */}
                <FaceBookSignInButton />
            </View>

            
            <View style={styles.registerHereContainer}>
                <TouchableOpacity
                    onPress={ () => navigation.navigate('Register')}
                    style={styles.registerHereLink}
                >
                    <Text style={styles.registerHereButton}>You dont have a account ? </Text>
                    <Text style={styles.registerHereButtonHere}>Register here 👽</Text>                
                </TouchableOpacity>
            </View>

        </View>
    </ScrollView>
  )
}

export default LoginScreen

const styles = StyleSheet.create({

    LoginScreenView: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'space-between', 
        alignItems: 'center',
        paddingHorizontal: 30,
        paddingVertical: 15,
        width: screenWidth,
        height: screenHeight
    },
    authLogoContainer: {
        width: "100%"
    },
    logo: {
        height: 250, 
        width: "auto"
    },
    logoTitle: {
        textAlign: 'center', 
        fontSize: 20,  
        textTransform: 'uppercase',
        fontWeight: 'bold'      
    },
    formContainer: {
        borderBottomColor: "black",
        borderBottomWidth: 1,
        paddingBottom: 20,
        paddingTop: 10
    },
    // loginBtn:  {
    //     textAlign:'center',
    //     justifyContent: 'center', 
    //     fontSize: '14',
    //     textTransform: 'uppercase',
    //     backgroundColor: 'blue'
    // },
    forgotten: {
        fontSize: 12,
        backgroundColor: 'transparent',
        paddingTop: 15,
    },
    forgottenHere: {
        fontSize: 12,
        backgroundColor: 'transparent',
        paddingTop: 15,
        color: "red"
    },
    registerHereButton: {
        fontSize: 12,
        backgroundColor: 'transparent'
    },
    registerHereButtonHere: {
        fontSize: 12,
        color: "teal"
    },
    registerHereLink: {
        flexDirection: 'row',
    }

})