import { View, Text, ScrollView, Image, StyleSheet, Button, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import { Dimensions } from 'react-native'

import CustomInput from '../../components/misc/CustomInput'
import CustomLoginButton from '../../components/misc/CustomLoginButton'
import GoogleSignInButton from '../../components/SignInSpecial/GoogleSignInButton'
import FaceBookSignInButton from '../../components/SignInSpecial/FaceBookSignInButton'



const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const LostPasswordScreen = ({ navigation }) => {


	const [email, setEmail] = useState('')
  

    function forgotPassword () {
        console.log('user logged')
    }


  return (
    <ScrollView style={{backgroundColor: "white"}}> 
        <View style={styles.LostPasswordScreenView}>
        
        {/* img */}
        <View style={styles.authLogoContainer}>
            <Image source={require('../../img/logo.jpg')} style={styles.logo}/>
            <Text style={styles.logoTitle}>Forgot password ? </Text>
        </View>

		<View style={{ width: 250, marginVertical: -40 }}>
			<Text style={styles.logoSubTitle}>Please remind us your email, a link to change your password will be sent to you 👽 </Text>
		</View>

        {/* form login */}
        <View style={styles.formContainer}>

            <CustomInput 
                placeholder={"email"} 
                value={email}
                onChange={(text) => setUsername(email)}                
            />
        
            <CustomLoginButton title='Send' />
        </View>   
        
        <View style={styles.registerHereContainer}>
            <TouchableOpacity
                onPress={ () => navigation.navigate('Login')}
                style={styles.registerHereLink}
            >
                <Text style={styles.registerHereButton}>Back to login page </Text>
                
                
            </TouchableOpacity>
        </View>
        


        </View>
    </ScrollView>
  )
}

export default LostPasswordScreen

const styles = StyleSheet.create({

    LostPasswordScreenView: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'space-evenly', 
        alignItems: 'center',
        paddingHorizontal: 30,
        paddingVertical: 15,
        width: screenWidth,
        height: screenHeight
    },
    authLogoContainer: {
        width: "100%"
    },
    logo: {
        height: 250, 
        width: "auto"
    },
    logoTitle: {
        textAlign: 'center', 
        fontSize: 20,  
        textTransform: 'uppercase',
        fontWeight: 'bold'      
    },
	logoSubTitle: {
		marginTop: 15
	},
    formContainer: {
        borderBottomColor: "black",
        borderBottomWidth: 1,
        paddingBottom: 30,
        paddingTop: 20
    },
    registerHereButton: {
        fontSize: 12,
        backgroundColor: 'transparent'
    },
    registerHereLink: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }

})