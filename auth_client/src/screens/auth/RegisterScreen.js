import { View, Text, ScrollView, Image, StyleSheet, Button, TouchableOpacity } from 'react-native'
import React, { useState, useContext } from 'react'
import { Dimensions } from 'react-native'
import CustomInput from '../../components/misc/CustomInput'
import { AuthContext } from '../../context/AuthContext'


const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const RegisterScreen = ({ navigation }) => {


    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordCheck, setPasswordCheck] = useState('')

    const {register} = useContext(AuthContext) 

    

  return (
    <ScrollView style={{backgroundColor: "white"}}> 
        <View style={styles.RegisterScreenView}>
        
        {/* img */}
        <View style={styles.authLogoContainer}>
            <Image source={require('../../img/logo.jpg')} style={styles.logo}/>
            <Text style={styles.logoTitle}>Register</Text>
        </View>

        {/* form login */}
        <View style={styles.formContainer}>  
            <CustomInput 
                placeholder={"Username"} 
                value={username}
                onChangeText={text => setUsername(text)}                
            />

            <CustomInput 
                placeholder={"Email"} 
                value={email}
                onChangeText={text => setUsername(text)}                
            />
        
            <CustomInput 
                placeholder={"Password"} 
                value={password} 
                onChangeText={text => setPassword(text)}
                secureTextEntry                
            />

			<CustomInput 
                placeholder={"Repeat password"} 
                value={passwordCheck} 
                onChangeText={text => setPassword(text)}  
                secureTextEntry                

            />
        
            
                <TouchableOpacity
                    // onPress={()=> console.log(email, password)}                        
                    onPress={()=>{
                        register(username, email, password, passwordCheck)
                    }}                      
                >                       
                    <Text style={styles.loginBtn}>Register </Text>
                </TouchableOpacity>
        </View> 

        
        <View style={styles.registerHereContainer}>
            <TouchableOpacity
                onPress={ () => navigation.navigate('Login')}
                style={styles.registerHereLink}
            >
                <Text style={styles.registerHereButton}>You have a account ? </Text>
                <Text style={styles.registerHereButton}>Login here 👽</Text>
                
            </TouchableOpacity>
        </View>
        


        </View>
    </ScrollView>
  )
}

export default RegisterScreen

const styles = StyleSheet.create({

    RegisterScreenView: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'space-between', 
        alignItems: 'center',
        paddingHorizontal: 30,
        paddingVertical: 15,
        width: screenWidth,
        height: screenHeight
    },
    authLogoContainer: {
        width: "100%"
    },
    logo: {
        height: 250, 
        width: "auto"
    },
    logoTitle: {
        textAlign: 'center', 
        fontSize: 20,  
        textTransform: 'uppercase',
        fontWeight: 'bold'      
    },
    formContainer: {
        borderBottomColor: "black",
        borderBottomWidth: 1,
        paddingBottom: 30,
        paddingTop: 20
    },
    registerHereButton: {
        fontSize: 12,
        backgroundColor: 'transparent'
    },
    registerHereLink: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }

})