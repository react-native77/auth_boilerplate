import { View, Text, TouchableOpacity } from 'react-native'
import React, { useContext } from 'react'
import { AuthContext } from '../context/AuthContext'

const HomeScreen = () => {


  	const {userToken, logout} = useContext(AuthContext) 

	return (
		<View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
			<Text>HomeScreen - {userToken ? `Hey user ${userToken}` : "User non identifié" } </Text>

			<View style={{marginTop: 30}}>
				<TouchableOpacity
					onPress={ () => logout()}
				>					
					<Text>Se déconnecter</Text>
				</TouchableOpacity>
			</View>

			

		</View>

    

  )
}

export default HomeScreen