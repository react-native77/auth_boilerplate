import React, {createContext, useState} from 'react';
import {BASE_URL_API} from '../config';
import axios from 'axios';



export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {

    const [ isLoading, setIsLoading ] = useState(false);
    const [ userToken, setUserToken ] = useState(null); 
    
    const register = ({username, email, password}) => {
        axios
        .post( `${BASE_URL_API}/users/register`, {
            username, 
            email, 
            password
        })
        .then(res => {
            let userInfos = res.data
            console.log(userInfos)
        })
        .catch(e => {
            console.log(`Erreur lors de l\'enregistrement : ${e}`)
        })


    }
    
    const login = ({email, password}) => {  
        console.log(email)        
        // const userLogin = Axios 
        //     .post('http://localhost:8080/users/login')        
        setUserToken('ImUserToken')
        setIsLoading(false)
    }

    const logout = () => {
        setUserToken(null)
        setIsLoading(false)
    }



    return (
        <AuthContext.Provider value={{login, logout, register, isLoading, userToken}} >
            {children}
        </AuthContext.Provider>
    );
}

