import { AuthContext } from '../context/AuthContext';
import React, { useContext } from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import AppStacks from '../stacks/AppStacks';
import AuthStacks from '../stacks/AuthStacks';


const AppNav = () => {

	const { isLoading, userToken } = useContext(AuthContext)

	if( isLoading ) {
		return(
			<View style={styles.indicator} >
				<ActivityIndicator size={'large'} />      		
			</View>
		)
	}

  return (
    <NavigationContainer >

        {/* <AuthStacks /> */}

        {userToken !== null ? <AppStacks /> : <AuthStacks />}        
    </NavigationContainer>	
  )
}

export default AppNav

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    indicator: {
      flex: 1, 
      justifyContent: "center",
      alignItems: "center"
    }
  });