import { View, Text, Image, StyleSheet } from 'react-native'
import React from 'react'

const FaceBookSignInButton = () => {
  return (
    <View style={styles.oauth}>
        <Image source={require('../../img/fbsignin.png')} style={styles.oauthLogo} />
    </View>
  )
}

export default FaceBookSignInButton

const styles = StyleSheet.create({
    oauth: {
        justifyContent: 'center',        
    }, 
    oauthLogo: {
        width: 250,
        height: 50
    }
})