import { View, Text, Image, StyleSheet } from 'react-native'
import React from 'react'



const GoogleSignInButton = () => {

    // GoogleSignin.configure();

  return (
    <View style={styles.oauth}>
      <Image source={require('../../img/google-signin-button.png')} style={styles.oauthLogo} />
    </View>
  )
}

export default GoogleSignInButton

const styles = StyleSheet.create({
    oauth: {
        justifyContent: 'center', 
        height: 30,       
    }, 
    oauthLogo: {
        width: 250,
        height: 30,
        marginBottom: 20
    }
})