import { View, Text, TextInput, StyleSheet } from 'react-native'
import React from 'react'

const CustomInput = ({placeholder}) => {
  return (
    <View>
      <TextInput 
        placeholder={placeholder}
        style={styles.customInput}
      />
    </View>
  )
}

export default CustomInput

const styles = StyleSheet.create({
    customInput: {
        width: 250,
        height: 40,
        borderWidth: 1,
        borderColor: "black",
        paddingHorizontal: 15,
        paddingVertical: 5,
        marginBottom: 15
    }
})