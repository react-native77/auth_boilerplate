import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../screens/auth/LoginScreen';
import SignOutScreen from '../screens/auth/SignOutScreen';
import HomeScreen from '../screens/HomeScreen'

const AppStacks = () => {

	const Stack = createNativeStackNavigator();

	return (
    <>
    <Stack.Navigator initialRouteName="Home">	
      <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}} />	
      <Stack.Screen name="Home" component={HomeScreen}  options={{headerShown: false}}/>
      <Stack.Screen name="Logout" component={SignOutScreen}  options={{headerShown: false}}/>
    </Stack.Navigator>
			
		</>
	)
}

export default AppStacks