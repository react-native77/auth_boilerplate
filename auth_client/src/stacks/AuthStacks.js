import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../screens/auth/LoginScreen';
import RegisterScreen from '../screens/auth/RegisterScreen';
import SignOutScreen from '../screens/auth/SignOutScreen';
import LostPasswordScreen from '../screens/auth/LostPasswordScreen';
import HomeScreen from '../screens/HomeScreen'

const AuthStacks = () => {

	const Stack = createNativeStackNavigator();

	return (
		<>
		<Stack.Navigator initialRouteName="Login">
			<Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}} />
			<Stack.Screen name="Register" component={RegisterScreen}  options={{headerShown: false}} />
			<Stack.Screen name="ForgotPassword" component={LostPasswordScreen}  options={{headerShown: false}}/>
			<Stack.Screen name="Home" component={HomeScreen}  options={{headerShown: false}}/>
			<Stack.Screen name="Logout" component={SignOutScreen}  options={{headerShown: false}}/>
		</Stack.Navigator>
			
		</>
	)
}

export default AuthStacks