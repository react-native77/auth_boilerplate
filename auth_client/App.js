import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import {AuthProvider} from './src/context/AuthContext'
import AppNav from "./src/nav/AppNav";

export default function App() {

	return (
		<AuthProvider>
			<AppNav />
		</AuthProvider>
	);
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
